#!/usr/bin/env -S gjs -m

import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Gio from 'gi://Gio';
import Gtk from 'gi://Gtk?version=4.0';
import Adw from 'gi://Adw';

Gio._promisify(Gio.File.prototype, 'replace_contents_bytes_async',
    'replace_contents_finish');

const TEST_HANDLERS_FILE = Gio.File.new_build_filenamev(
    [GLib.get_user_data_dir(), 'xdg-handlers.json']);
const TEST_HANDLERS_BYTES = new GLib.Bytes(JSON.stringify({
  'org.gnome.OpenUri': [
    {
      'hosts': ['openstreetmap.org', '*.openstreetmap.org'],
      'paths': ['/?lat=*&lon=*', '/?#map=*', '/#map=*', '/node/*', '/way/*'],

      'domains': ['openstreetmap.org/?q='],
    },
    {
      'hosts': ['maps.apple.com'],
      'paths': ['/*,*', '/?q=*'],

      'domains': ['maps.apple.com/?q=', 'map.apple.com'],
    }
  ],
  'org.gnome.Fractal': [
    {
      'schemes': ['http?'],
      'hosts': ['matrix.to'],
      'paths': ['/#/#*:*'],

      'domains': ['matrix.to/#/#'],
    }
  ],
  'org.gnome.Software': [
    {
      'hosts': ['flathub.org', '*.flathub.org'],
      'paths': ['/apps/search?q=*', '/apps/*'],

      'domains': ['flathub.org/apps'],
    }
  ],
  'io.freetubeapp.FreeTube': [
    {
      'schemes': ['http', 'https'],
      'hosts': ['youtu.be'],

      'domains': ['youtu.be'],
    },
    {
      'schemes': ['http', 'https'],
      'hosts': ['youtube.com', '*.youtube.com'],
      'paths': ['/watch?v=*'],

      'domains': ['youtube.com/watch?v='],
    }
  ],
  'org.jitsi.jitsi-meet': [
    {
      'schemes': ['http', 'https'],
      'hosts': ['meet.jit.si'],

      'domains': ['meet.jit.si'],
    }
  ]
}, null, 4));

const TEST_INSTRUCTIONS = `
Build the project, then start the service and restart the appropriate
backend and keyring service if necessary (e.g. Fractal account)
<span font-family="monospace">
meson setup --prefix=/usr _build &amp;&amp; meson compile -C _build
./_build/src/xdg-desktop-portal --replace &amp;
systemctl --user restart xdg-desktop-portal-gnome
gnome-keyring-daemon --replace
</span>
For toolbox start xdg-desktop-portal with flatpak-spawn:
<span font-family="monospace">
flatpak-spawn --host _build/src/xdg-desktop-portal --replace &amp;
</span>`;

const TEST_URIS = {
    'Maps': [
        'https://www.openstreetmap.org/#map=18/37.8817460/-122.1875468',
        'http://openstreetmap.org/way/34857834/',
        'https://openstreetmap.org/?lat=37.88181&lon=-122.18740&zoom=18',
        'geo:37.88181,-122.18740',
        'geo:39.74524,-105.00614?z=19', // excluded
        'maps:q=GNOME+Foundation',
    ],
    'Matrix': [
        'https://matrix.to/#/#fractal:gnome.org',
        'https://matrix.to/#/#xdg-desktop-portals:matrix.org',
    ],
    'Software': [
        'https://flathub.org/apps/re.sonny.Workbench',
        'https://flathub.org/apps/search?q=biblioteca',
        'https://flathub.org/about',
    ],
    'YouTube': [
        'https://youtu.be/r_QyRJf3rtQ',
        'https://www.youtube.com/watch?v=hvY_BYHfurc',
    ],
    'Jitsi': [
        'https://meet.jit.si/STF_❤️_GNOME_and_GNOME_❤️_STF',
        'jitsi-meet://meet.jit.si/STF_❤️_GNOME_and_GNOME_❤️_STF',
        'https://jitsi.org/downloads/',
    ],
};

class ExampleApp extends Adw.Application {
    static {
        GObject.registerClass(this);
    }

    vfunc_activate() {
        const window = new Adw.PreferencesDialog();

        const page = new Adw.PreferencesPage();
        window.add(page);

        /*
         * Setup
         */
        const setupGroup = new Adw.PreferencesGroup({
            title: 'Handler Configuration Setup',
        });
        page.add(setupGroup);

        const descriptionRow = new Adw.ExpanderRow({
            title: 'Pull request: <a href="https://github.com/flatpak/xdg-desktop-portal/pulls/1313">flatpak/xdg-desktop-portal#1313</a>',
            subtitle: 'Expand for build instructions',
            use_markup: true,
        });
        setupGroup.add(descriptionRow);
        const descriptionLabel = new Gtk.Label({
            label: TEST_INSTRUCTIONS,
            selectable: true,
            use_markup: true,
            wrap: true,
        });
        descriptionRow.add_row(descriptionLabel);

        const setupRow = new Adw.ActionRow({
            title: 'Example Configuration File',
            subtitle: 'Install JSON file to ~/.local/share/',
        });
        setupGroup.add(setupRow);
        const setupButtons = new Gtk.Box({
            spacing: 12,
            valign: Gtk.Align.CENTER,
        });
        setupRow.add_suffix(setupButtons);
        const setupEdit = new Gtk.Button({
            label: 'Edit',
        });
        setupEdit.connect('clicked', () => {
            Gtk.FileLauncher
                .new(TEST_HANDLERS_FILE)
                .launch(null, null, null);
        });
        setupButtons.append(setupEdit);
        const setupInstall = new Gtk.Button({
            label: 'Install',
        });
        setupInstall.connect('clicked', () => {
            setupInstall.sensitive = false;
            TEST_HANDLERS_FILE
                .replace_contents_bytes_async(TEST_HANDLERS_BYTES, null, false,
                    Gio.FileCreateFlags.REPLACE_DESTINATION, null)
                .finally(() => {
                    setupInstall.sensitive = true;
                });
        });
        setupButtons.append(setupInstall);

        /*
         * URI Entry
         */
        const entryGroup = new Adw.PreferencesGroup();
        page.add(entryGroup);

        const linkEntry = new Adw.EntryRow({
            title: 'Activate to Open',
            text: 'maps:q=GNOME+Foundation'
        });
        linkEntry.connect('entry-activated', () => {
            Gtk.UriLauncher
                .new(linkEntry.text)
                .launch(null, null, null);
        });
        entryGroup.add(linkEntry);

        /*
         * URI Presets
         */
        const presetsGroup = new Adw.PreferencesGroup({
            title: 'Test URIs',
        });
        page.add(presetsGroup);
        const randomButton = new Gtk.Button({
            child: new Adw.ButtonContent({
                label: 'Random URI',
                icon_name: 'adw-external-link-symbolic',
            }),
            valign: Gtk.Align.CENTER,
        });
        randomButton.connect('clicked', () => {
            const uris = [...Object.values(TEST_URIS)];
            linkEntry.text = uris[Math.floor(Math.random() * uris.length)];
            Gtk.UriLauncher.new(linkEntry.text).launch(null, null, null);
        });
        presetsGroup.set_header_suffix(randomButton);

        for (const [name, uris] of Object.entries(TEST_URIS)) {
            const uriGroup = new Adw.ExpanderRow({
                title: name,
            });
            presetsGroup.add(uriGroup);
            for (const uri of uris) {
                const uriRow = new Adw.ActionRow({
                    title: `<a href="${GLib.markup_escape_text(uri, -1)}">${GLib.markup_escape_text(uri, -1)}</a>`,
                    use_markup: true,
                });
                uriGroup.add_row(uriRow);
            }
        }

        window.present(null);
        window.connect('unmap', () => this.release());
        this.hold();
    }
}

new ExampleApp().run([imports.system.programInvocationName].concat(ARGV));
